#include<stdio.h>
#include<omp.h>

int main (void){
  int i;
  #pragma omp paralell
  {
    printf("Hola Mundo\n");
    for (i=0;i<10;i++){
      printf("Iteracion [%d]\n", i);
    }
  }// fin de la region paralela
  printf("Adios\n");

  return 0;
}
// fin del prog
